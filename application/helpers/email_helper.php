<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

function send_email($to = '', $message = '', $subject = '', $from = 'sajjadbuttg@gmail.com', $from_name = 'vFixit') {
  $to = ($to) ? $to : 'sajjadbuttg@gmail.com';
  if ($to) {
    $config = email_settings();

    $CI = get_instance();
    $CI->load->library('email');
    $CI->email->set_mailtype('html');
    $CI->email->set_newline("\r\n");
    $CI->email->from($from, $from_name);
    $CI->email->to($to);
    $CI->email->subject($subject);
    $CI->email->message($message);
    $send = $CI->email->send();
    return $send;
  }
}

function email_settings() {
  $config = Array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://smtp.googlemail.com',
      'smtp_port' => 465,
      'smtp_user' => 'sajjadbuttg@gmail.com',
      'smtp_pass' => 'jallo123',
      'mailtype' => 'html',
      'charset' => 'iso-8859-1'
  );
  return $config;
}

function contact_email($user, $msg){
  $message = 'From: ' . $user->first_name . ' ' . $user->last_name . '<br>';
  $message .= 'Email: ' . $user->email . '<br>';
  $message .= 'Phone number: ' . $user->pnumber . '<br>';
  $message .= 'Message: ' . $msg . '<br>';
  send_email('', $message, 'Contact', $user->email, $user->first_name . ' ' . $user->last_name);
}

function booking_email($user, $service, $payment_mode){
  $payment_modes = array('credit_card' => 'Credit Card', 'cash' => 'Cash');
  $services = array('1' => 'House Keeping', '2' => 'Electrician', '3' => 'Plumber', '4' => 'Mechanic');
  $message = 'From: ' . $user->first_name . ' ' . $user->last_name . '<br>';
  $message .= 'Email: ' . $user->email . '<br>';
  $message .= 'Phone number: ' . $user->pnumber . '<br>';
  $message .= 'Service: ' . $services[$service] . '<br>';
  $message .= 'Payment Mode: ' . $payment_modes[$payment_mode] . '<br>';
  return send_email('', $message, 'Booking', $user->email, $user->first_name . ' ' . $user->last_name);
}