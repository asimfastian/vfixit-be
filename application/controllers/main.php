<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class main extends REST_Controller {

  function signup_get(){
    $this->load->database();
    
    $email = $this->get('email');
    $pass = $this->get('pass');
    $fname = $this->get('fname');
    $lname = $this->get('lname');
    $pnumber = $this->get('pnumber');

    $this->db->where('email', $email);
    $this->db->from('users');
    if($this->db->count_all_results()){
      $this->response(array('success' => FALSE, 'msg' => 'Email address already exists.'), 200);
    }

    $this->db->insert('users', array('email' => $email, 'password' => md5($pass), 'first_name' => $fname, 'last_name' => $lname,
        'pnumber' => $pnumber));
    $id = $this->db->insert_id();

    $this->response(array('success' => TRUE, 'uid' => $id), 200);
  }

  function login_get(){
    $this->load->database();
    
    $email = $this->get('email');
    $pass = $this->get('pass');

    $this->db->where('email', '' . $email);
    $this->db->where('password', '' . md5($pass));
    $query = $this->db->from('users')->get();
    $user = $query->row();
    unset($user->password);

    if($user->id){
      $this->response(array('success' => TRUE, 'user' => $user), 200);
    }

    $this->response(array('success' => FALSE, 'msg' => 'Email address or password you entered is incorrect.'), 200);
  }

  function contact_get() {
    $this->load->model('main_model');
    $uid = $this->get('uid');
    $msg = $this->get('say');

    $this->main_model->contact($uid, $msg);
    $this->response(array('success' => TRUE, 'uid' => $uid), 200);
  }

  function booking_get(){
    $this->load->model('main_model');
    $uid = $this->get('uid');
    $service = $this->get('service');
    $payment_mode = $this->get('payment_mode');

    $resp = $this->main_model->booking($uid, $service, $payment_mode);
    $this->response(array('success' => TRUE, 'msg' => 'Thank you! you will receive a call shortly.', 'resp' => $resp), 200);
  }

  function tips_get(){
    $this->load->model('main_model');

    $tips = $this->main_model->tips();
    $this->response(array('success' => TRUE, 'tips' => $tips), 200);
  }

}