<?php

class main_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  function get_user($uid = 0) {
    $this->load->database();
    $this->db->where('id', "$uid");
    $query = $this->db->from('users')->get();
    $user = $query->row();
    return $user;
  }

  function contact($uid = 0, $msg = '') {
    $this->load->helper('email_helper');

    $user = $this->get_user($uid);
    
    if($user->email){
      contact_email($user, $msg);
    }
  }

  function booking($uid = 0, $service = '', $payment_mode = '') {
    $this->load->helper('email_helper');

    $user = $this->get_user($uid);
    
    if($user->email){
      return booking_email($user, $service, $payment_mode);
    }
    return 'no email';
  }

  function tips() {
    return array(
        'No Credit Card? Its alright, you can pay at your doorstep',
        'Gather loyalty points and enjoy discounts when you order via vFixit',
        'The application is multilingual – choose what suits you',
        'For more information on rates and service – call @ 55275461');
  }
}